package Uppgift1;


import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class solution {
	public static void main(String[] args) {

		List<Person> persons = List.of(
				new Person("Gertrud","Female", 45000),
				new Person("Niklas","Male", 33000),
				new Person("Lisbeth","Female", 38000),
				new Person("Markus","Male", 40000),
				new Person("Johnny","Male", 29000),
				new Person("Kim","Female", 25000),
				new Person("Ingrid","Female", 31000),
				new Person("Lars","Male", 26000),
				new Person("Kent","Male", 50000),
				new Person("Elisabeth","Female", 35000)
		);

		Map<String, Double> map = persons
				.stream()
				.collect(Collectors.groupingBy(Person::getGender, Collectors.averagingDouble(Person::getSalary)));

		System.out.println("-------------------------------");
		System.out.println("Average salary by gender:");
		System.out.println("Males "+map.get("Male"));
		System.out.println("Females "+map.get("Female"));

		System.out.println("-------------------------------");
		System.out.println("Highest Salary");

		Optional<Person> highestSalary = persons
				.stream()
				.max(Comparator.comparing(Person::getSalary));

		System.out.println(highestSalary.get());

		System.out.println("-------------------------------");
		System.out.println("Lowest Salary");

		Optional<Person> lowestSalary = persons
				.stream()
				.min(Comparator.comparing(Person::getSalary));

		System.out.println(lowestSalary.get());
		System.out.println("-------------------------------");



	}
}
