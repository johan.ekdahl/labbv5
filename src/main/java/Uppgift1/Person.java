package Uppgift1;

public class Person {
	private String name;
	private String gender;
	private double salary;

	public String getName() {
		return name;
	}

	public String getGender() {
		return gender;
	}

	public double getSalary() {
		return salary;
	}

	public Person(String name, String gender, double salary) {
		this.name = name;
		this.gender = gender;
		this.salary = salary;
	}

	@Override
	public String toString() {
		return "User{" +
				"name='" + name + '\'' +
				", gender='" + gender + '\'' +
				", salary=" + salary +
				'}';
	}
}
