package Uppgift3;

import java.util.List;
import java.util.regex.Pattern;

public class solution3 {
	public static void main(String[] args) {

		List<String> listOfWords = List.of("struck","announced","dig","damage","impossible","waste",
				"journey","straight","luck","scene","secret","help",
				"fireplace","jump","own","order","lake","danger",
				"huge","state","news","oxygen","halfway","truth",
				"program","inch","century","track","port","speak",
				"avoid","group","combination","slIdE","bEll","grxdx");


		Pattern pattern1 = Pattern.compile("[aeiouy].*[aeiouy]",Pattern.CASE_INSENSITIVE);
		Pattern pattern2 = Pattern.compile("(\\w*[aeiouy]){2,}",Pattern.CASE_INSENSITIVE);

		for(String word : listOfWords){
			if(pattern1.matcher(word).find()){
				System.out.println(word);
			}
		}
		System.out.println("--------------------------------------------------");

		listOfWords
				.stream()
				.filter(word -> pattern2.matcher(word).find())
				.forEach(System.out::println);


	}
}
