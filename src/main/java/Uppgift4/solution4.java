package Uppgift4;


public class solution4 {
	public static void main(String[] args) throws InterruptedException {


		Thread thread1 = new Thread(() -> {
			for(int i = 0; i < 350000; i++){

				int counter = 0;
				for(int j = i; j>=1; j--){
					if(i%j==0){
						counter++;
					}
				}
				if(counter == 2){
					System.out.println(i);
				}
			}


		});
		Thread thread2 = new Thread(() -> {
			for(int i = 350000; i < 500000; i++){

				int counter = 0;
				for(int j = i; j>=1; j--){
					if(i%j==0){
						counter++;
					}
				}
				if(counter == 2){
					System.out.println(i);
				}
			}
		});



		thread1.start();
		thread2.start();
		thread1.join();
		thread2.join();
		System.out.println("Färdig");



	}
}
