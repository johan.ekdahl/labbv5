package Uppgift2;

public class solution2 {
	public static void main(String[] args) {

		VolvoFactory volvoFactory = new VolvoFactory();
		Car carXC60 = volvoFactory.newModel("xc60");
		System.out.println("--------------------------------------");
		Car carV90 = volvoFactory.newModel("v90");
		System.out.println("--------------------------------------");

		System.out.println("Getting: "+carXC60.getDesign());
		System.out.println("Getting: "+carXC60.getPlan());
		System.out.println("Getting: "+carXC60.getService());
		System.out.println("Getting: "+carXC60.getManufacture());
		System.out.println("Getting: "+carXC60.getSale());
		System.out.println("Getting total cost for car: "+carXC60.getTotalcost());


		System.out.println("--------------------------------------");

		System.out.println("Getting: "+carV90.getDesign());
		System.out.println("Getting: "+carV90.getPlan());
		System.out.println("Getting: "+carV90.getService());
		System.out.println("Getting: "+carV90.getManufacture());
		System.out.println("Getting: "+carV90.getSale());
		System.out.println("Getting total cost for car: "+carV90.getTotalcost());
	}
}
