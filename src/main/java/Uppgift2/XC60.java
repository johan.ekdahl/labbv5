package Uppgift2;

public class XC60 extends Car {
	private String plan = "";
	private String design = "";
	private String manufacture = "";
	private String sale = "";
	private String service = "";
	private int totalcost = 0;

	public int getTotalcost() {
		return totalcost;
	}

	public void setTotalcost(int totalcost) {
		this.totalcost += totalcost;
	}

	private void setPlan() {
		this.plan = "Plan for Suvcar";
	}

	private void setDesign() {
		this.design = "Design for Suvcar";
	}

	private void setManufacture() {
		this.manufacture = "Plant to manufacture Suvcar";
	}

	private void setSale() {
		this.sale = "Dealership agreement for Suvcar";
	}

	private void setService() {
		this.service = "Servicebusiness for Suvcar";
	}

	public String getPlan() {
		return plan;
	}

	public String getDesign() {
		return design;
	}

	public String getManufacture() {
		return manufacture;
	}

	public String getSale() {
		return sale;
	}

	public String getService() {
		return service;
	}

	@Override
	public void Plan() {
		System.out.println("Planning car with SUV characteristics");
		setPlan();

		setTotalcost(30000);
	}

	@Override
	public void Design() {
		System.out.println("Designing car with SUV characteristics");
		setDesign();
		setTotalcost(20000);
	}

	@Override
	public void Manufacture() {
		System.out.println("Manufacture car with SUV characteristics");
		setManufacture();
		setTotalcost(50000);
	}

	@Override
	public void Sale() {
		System.out.println("Bringing SUV car to dealership");
		setSale();
		setTotalcost(60000);
	}

	@Override
	public void Service() {
		System.out.println("Service car with SUV tires and changing oil");
		setService();
		setTotalcost(20000);
	}
}
