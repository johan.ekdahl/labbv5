package Uppgift2;

public class VolvoFactory extends CarFactory {
	Car car;

	@Override
	Car createCar(String model) {
		switch (model.toUpperCase()) {
			case "XC60" -> car = new XC60();
			case "V90" -> car = new V90();
			default -> car = null;
		}
		return car;
	}
}
