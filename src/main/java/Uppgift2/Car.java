package Uppgift2;

abstract public class Car {
	public abstract void Plan();
	public abstract void Design();
	public abstract void Manufacture();
	public abstract void Sale();
	public abstract void Service();

	abstract String getDesign();
	abstract String getPlan();
	abstract String getManufacture();
	abstract String getService();
	abstract String getSale();
	abstract int getTotalcost();

}
