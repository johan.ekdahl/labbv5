package Uppgift2;

abstract public class CarFactory {

	Car newModel(String model){
		Car car = createCar(model);
		car.Plan();
		car.Design();
		car.Manufacture();
		car.Sale();
		car.Service();
		return car;
	}

	abstract Car createCar(String model);
}
