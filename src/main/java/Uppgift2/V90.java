package Uppgift2;

public class V90 extends Car {
	private String plan = "";
	private String design = "";
	private String manufacture = "";
	private String sale = "";
	private String service = "";
	private int totalcost = 0;

	public String getPlan() {
		return plan;
	}

	public String getDesign() {
		return design;
	}

	public String getManufacture() {
		return manufacture;
	}

	public String getSale() {
		return sale;
	}

	public String getService() {
		return service;
	}

	public int getTotalcost() {
		return totalcost;
	}

	private void setPlan() {
		this.plan = "Plan for Kombicar";
	}

	private void setDesign() {
		this.design = "Design for Kombicar";
	}

	private void setManufacture() {
		this.manufacture = "Plant to manufacture Kombicar";
	}

	private void setSale() {
		this.sale = "Dealership agreement for Kombicar";
	}

	private void setService() {
		this.service = "Servicebusiness for Kombicar";
	}

	private void setTotalcost(int totalcost) {
		this.totalcost += totalcost;
	}

	@Override
	public void Plan() {
		System.out.println("Planning car with Kombi characteristics");
		setPlan();
		setTotalcost(35000);
	}

	@Override
	public void Design() {
		System.out.println("Designing car with lots of trunk space");
		setDesign();
		setTotalcost(25000);

	}

	@Override
	public void Manufacture() {
		System.out.println("Manufacture car with Kombi characteristics");
		setManufacture();
		setTotalcost(100000);

	}

	@Override
	public void Sale() {
		System.out.println("Bringing Kombi car to dealership");
		setSale();
		setTotalcost(40000);

	}

	@Override
	public void Service() {
		System.out.println("Service car with Kombi tires and changing oil");
		setService();
		setTotalcost(20000);
	}
}
